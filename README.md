# Mobile Application Developer test project 

## Úvod

Vítáme vás v našem testovacím projektu a jsme velmi rádi, že jste se rozhodl/a absolvovat tento vstupní projekt na pozici Mobile Application Developer. Jen upřesňujeme, že se nejedná o placený projekt, ale pouze o vstupní testovací projekt, na základě kterého získáme představu o vašich znalostech. Naše mobilní aplikace je psaná s využitím Angular2+ / Ionic a tedy HTML5 a komunikuje s REST API, proto je toto zadání soustředěno právě tímto směrem.

## Instrukce

* Vytvořte statickou webovou aplikaci podle vzoru. Všechny potřebné soubory a obrázky jsou nahrané v tomto repozitáři. Vaším cílem je vytvořit webové stránky tak, aby odpovídaly navrženému designu a zároveň byly responzivní. Formulář s komentáři by měl být funkční (bez serverové části, pouze v JavaScriptu).

* V průběhu vývoje myslete na to, že by měly být použity "best practices" frontend vývoje. 
###  1. fáze

Vytvořte statické stránky podle grafického návrhu v tomto repozitáři.

###  2. fáze

Seznam StarWars filmů naplňte přes dotaz do REST API, který najdete na adrese https://swapi.co/ a vypište title, director a release_date(ve formátu DD.MM.YYYY).

###  3. fáze

Zprovozněte komentáře tak, aby se validovalo alespoň prázdné pole. Komentáře ukládejte do localstorage prohlížeče.

###  4. fáze

Vytvořte mapu přes knihovnu Leaflet (https://leafletjs.com/), případně Google Maps API a kdekoliv zobrazte marker s popup oknem a červený kroužek.


## Doručení

* Až budete hotov, pozvěte [ondrej.berger@level.systems](mailto:ondrej.berger@level.systems) a [karel.ulman@level.systems](mailto:karel.ulman@level.systems) do vašeho repozitáře v gitu (např. GitHub, Bitbucket) a pošlete odkaz na živé demo, které nahrajte veřejně a to pouze na dobu 7 dnů. 

* V případě jakýchkoliv dotazů, pište na [developers@level.systems](mailto:developers@level.systems)


## Upřesnění

* Veškeré ikony na webu pocházejí z knihovny FontAwesome https://fontawesome.com/ (pokud v seznamu nenajdete shodnou ikonu, použíjte jinou, podobnou).

* K instalaci knihoven použíjte ideálně NPM.

* Stránka by po kliknutí na menu a tlačítka měla odscrolovat na požadovaný obsah.

* Spuštění a build aplikace pro vývojáře by měly být dokumentovány.

* Designově vycházejte z Bootstrap4 - https://getbootstrap.com.

* Kódujte a stylujte dokument podle standardů W3C.

* Použijte alespoň jQuery případně Angular2+.

* Rádi uvidíme nějaký stylovací preprocessor (LESS, SASS) a bundler (Webpack).

* Web by měl být mobile friendly / responzivní.

* Projekt je velmi otevřený, v kreativitě řešení se meze nekladou. Překvapte nás!

* Doba na projektu by neměla přesáhnout cca 1 den čistého času (tj. 8 hod. práce).




